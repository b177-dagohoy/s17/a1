let enrollees = [];

function addStudent(student){
	enrollees.push(student.toLowerCase());
	console.log(student + " was added to the student's list");
};


function countStudents(){
	console.log('There are total of ' + enrollees.length + ' students enrolled')
}

function printStudents(){
	enrollees.forEach(function(student){
		console.log(student);
	})
}


function findStudent(name){
	let result = enrollees.filter(function(student){
		return student.includes(name.toLowerCase());
	})
	if(result.length >= 2){
		console.log(result.toString() + ' are enrollees')
	}
	else if(result.length == 1){
		console.log(name + ' is an Enrollee')
	}
	else{
		console.log('No student found with the name '+ name)
	}
}